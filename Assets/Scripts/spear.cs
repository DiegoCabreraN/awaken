﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spear : MonoBehaviour
{
    public Transform destination;
    public GameObject spears;

    private void OnTriggerEnter(Collider other)
    {
        spears.transform.position = destination.position;
    }

}
