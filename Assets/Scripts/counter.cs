﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class counter : MonoBehaviour
{ 
    public void Waitfor(int i)
    {
        StartCoroutine(Wait(i));
    }
    public void Off() {
        //this.gameObject.GetComponent<Animator>().SetBool("Start", false);
        this.gameObject.SetActive(false);
    }

    IEnumerator Wait(int i)
    {
        yield return new WaitForSeconds(i);
        this.gameObject.GetComponent<Animator>().SetBool("Start", true);
    }
    
}
