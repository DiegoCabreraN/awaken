﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventBehaviour : MonoBehaviour
{
    public EventsTrigger EventObject;
    private Vector3[] originalPosition;
    public EventSystemManager eventSystemManager;
    public int eventIndex;
    public bool o = false;

    void Start()
    {
        originalPosition = new Vector3[EventObject.objectToTransform.Length];
        for (int i = 0; i < EventObject.objectToTransform.Length; i++)
        {
            originalPosition[i] = EventObject.objectToTransform[i].transform.localPosition;
 
        }
    }
    void Update()
    {
        if (EventObject.moving)
        { 
            for (int i = 0; i < EventObject.objectToTransform.Length; i++)
            { 
                if (EventObject.objectToTransform[i] != null)
                {
                     
                    if (EventObject.objectToTransform[i].transform.localPosition.y > originalPosition[i].y * 4.2)
                    {
                        
                        EventObject.objectToTransform[i].transform.Translate(Vector3.up * originalPosition[i].y * 1.2f * Time.deltaTime);
                    } 
                    else
                    {
                        if (o)
                        {
                            if (EventObject.objectToTransform[i].transform.localPosition.y > originalPosition[i].y * -1.5)
                            {

                                EventObject.objectToTransform[i].transform.Translate(Vector3.down * originalPosition[i].y * 1.2f * Time.deltaTime);
                            }
                            else
                            {
                                if (EventObject.destroyable)
                                { 
                                    eventSystemManager.discoverEvent(eventIndex);
                                    Destroy(EventObject.objectToTransform[i]);
                                    EventObject.objectToTransform[i] = null;
                                    
                                }   
                            }
                        }  
                        else if (EventObject.destroyable)
                        {
                            eventSystemManager.discoverEvent(eventIndex);
                            Destroy(EventObject.objectToTransform[i]);
                            EventObject.objectToTransform[i] = null; 
                        }
                    }
                }
            }
        } 
        else if (EventObject.appear)
        {
            for(int i = 0; i < EventObject.objectToTransform.Length;i++) {
                EventObject.objectToTransform[i].SetActive(true);
            }
        }
        else
        {
            if (EventObject.triggerType == EventsTrigger.TriggerType.Lever)
            {
                for (int i = 0; i < EventObject.objectToTransform.Length; i++)
                {
                    if (EventObject.objectToTransform[i] != null)
                    {
                        if (EventObject.objectToTransform[i].transform.localPosition.y < originalPosition[i].y)
                        {
                            EventObject.objectToTransform[i].transform.Translate(Vector3.down * originalPosition[i].y * 1.2f * Time.deltaTime);
                        }
                    }
                }
            }
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (EventObject.triggerType == EventsTrigger.TriggerType.Button)
        {
            if (other.CompareTag("Player"))
            {
                if (EventObject.triggerObjective == EventsTrigger.EventTriggerObjective.OpenDoor)
                {
                    StartMoving();
                }
                else if (EventObject.triggerObjective == EventsTrigger.EventTriggerObjective.DisappearObject)
                {
                    EventObject.disappear = true;
                }
                else if (EventObject.triggerObjective == EventsTrigger.EventTriggerObjective.AppearObject)
                {
                    EventObject.appear = true;
                }

                EventObject.buttonTop.transform.localPosition = new Vector3(EventObject.buttonTop.transform.localPosition.x, EventObject.buttonTop.transform.localPosition.y - .1f, EventObject.buttonTop.transform.localPosition.z);
            }
        } 
    }
    private void OnTriggerExit(Collider other)
    {
        if (EventObject.triggerType == EventsTrigger.TriggerType.Button)
        { 
            if (EventObject.triggerObjective == EventsTrigger.EventTriggerObjective.DisappearObject)
            {
                EventObject.disappear = false;
            }
            else if (EventObject.triggerObjective == EventsTrigger.EventTriggerObjective.AppearObject)
            {
                EventObject.appear = false;
            }
            EventObject.buttonTop.transform.localPosition = new Vector3(EventObject.buttonTop.transform.localPosition.x, EventObject.buttonTop.transform.localPosition.y + .1f, EventObject.buttonTop.transform.localPosition.z);
        }
    }
    public void StartMoving() {
        EventObject.moving = true;
    }
    public void StopMoving()
    {
        EventObject.moving = false;
    }
}
