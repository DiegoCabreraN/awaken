﻿using UnityEditor;
using UnityEngine;
using UnityEditor.SceneManagement;

[CustomEditor(typeof(MasterSystem))]
[System.Serializable]
public class MasterSystemEditor : Editor
{
    [SerializeField]
    MasterSystem master;
    [SerializeField]
    Object eventSystem, settingsMenu, settingSystem, playerControl;
    bool events, config, movement;

    private void OnEnable()
    {
        master = (MasterSystem)target;
        eventSystem = master.eventSystem;
        settingsMenu = master.SettingsMenu;
        settingSystem = master.settingSystem;
        playerControl = master.playerControl;
        events = true;
        config = true;
        movement = true;
    }

    public override void OnInspectorGUI()
    {
        var Title = new GUIStyle(GUI.skin.label) { alignment = TextAnchor.MiddleCenter, fontSize = 14, fontStyle = FontStyle.Bold, wordWrap = false };
        var Subheader = new GUIStyle(GUI.skin.label) { alignment = TextAnchor.MiddleCenter, fontSize = 12 };
        var normalLabel = new GUIStyle(GUI.skin.label) { alignment = TextAnchor.MiddleCenter, fontSize = 11 };
        var foldOut = new GUIStyle(EditorStyles.foldout) { stretchWidth = true, fixedWidth = 1 }; 

        GUILayout.Space(20);
        EditorGUILayout.LabelField("Sistema Maestro", Title);
        GUILayout.Space(20);
        EditorGUILayout.LabelField("", GUI.skin.horizontalSlider);
        GUILayout.Space(20);
        GUILayout.BeginHorizontal();
        GUILayout.Label("Sistema de Eventos:", Subheader, GUILayout.ExpandWidth(true));
        GUILayout.EndHorizontal();
        GUILayout.BeginHorizontal();
        events = EditorGUILayout.Foldout(events, "", foldOut);
        GUILayout.EndHorizontal();
        if (events)
        {
            GUILayout.BeginHorizontal();
            eventSystem = EditorGUILayout.ObjectField(eventSystem, typeof(EventSystemManager), true, GUILayout.ExpandWidth(true)); 
            GUILayout.EndHorizontal();
            GUILayout.Space(20);
        }
        EditorGUILayout.LabelField("", GUI.skin.horizontalSlider);


        GUILayout.Space(20);
        GUILayout.BeginHorizontal();
        GUILayout.Label("Sistema de Configuracion:", Subheader, GUILayout.ExpandWidth(true));
        GUILayout.EndHorizontal();
        GUILayout.BeginHorizontal();
        config = EditorGUILayout.Foldout(config, "", foldOut);
        GUILayout.EndHorizontal();
        if (config)
        {
            GUILayout.BeginHorizontal();
            settingsMenu = EditorGUILayout.ObjectField(settingsMenu, typeof(GameObject), true, GUILayout.ExpandWidth(true)); 
            settingSystem = EditorGUILayout.ObjectField(settingSystem, typeof(SettingSystem), true, GUILayout.ExpandWidth(true)); 
            GUILayout.EndHorizontal();
            GUILayout.Space(20);
        }
        EditorGUILayout.LabelField("", GUI.skin.horizontalSlider);

        GUILayout.Space(20);
        GUILayout.BeginHorizontal();
        GUILayout.Label("Sistema de Movimiento:", Subheader, GUILayout.ExpandWidth(true));
        GUILayout.EndHorizontal();
        GUILayout.BeginHorizontal();
        movement = EditorGUILayout.Foldout(movement, "", foldOut);
        GUILayout.EndHorizontal();
        if (movement)
        {
            GUILayout.BeginHorizontal();
            playerControl = EditorGUILayout.ObjectField(playerControl, typeof(PlayerControllerSystem), true, GUILayout.ExpandWidth(true)); 
            GUILayout.EndHorizontal();
            GUILayout.Space(20);
        } 
        EditorGUILayout.LabelField("", GUI.skin.horizontalSlider);

        /*a = EditorGUILayout.Foldout(a, "Cambiar variables");
        if (a)
        {
            base.OnInspectorGUI();
        }*/
        if (GUI.changed)
        { 
            EditorUtility.SetDirty(master);
            EditorSceneManager.MarkSceneDirty(master.gameObject.scene);
            ApplyChanges();
        }

    }
    void ApplyChanges() {
        foreach (GameObject obj in Selection.gameObjects)
        {
            MasterSystem ms = obj.GetComponent<MasterSystem>();
            if (ms != null)
            { 
                ms.eventSystem = (EventSystemManager)eventSystem;
                ms.SettingsMenu = (GameObject)settingsMenu;
                ms.settingSystem = (SettingSystem)settingSystem;
                ms.playerControl = (PlayerControllerSystem)playerControl;
            }
        } 
    }
}
