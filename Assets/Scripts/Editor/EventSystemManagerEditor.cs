﻿using System.Collections; 
using UnityEngine;
using UnityEditor;
using UnityEngine.EventSystems;

[CustomEditor(typeof(EventSystemManager))]
public class EventSystemManagerEditor : Editor
{
    public EventSystemManager ESM;
    public Object lightSystem, soundSystem, riddleSystem, eventSystem;
    public SerializedObject ESMSerialized;

    private void OnEnable()
    {
        ESM = (EventSystemManager)target; 
        eventSystem = ESM.es; 
    }
    public override void OnInspectorGUI()
    {
        #region Label Styles
        var Title = new GUIStyle(GUI.skin.label) { alignment = TextAnchor.MiddleCenter, fontSize = 14, fontStyle = FontStyle.Bold, wordWrap = false };
        var Subheader = new GUIStyle(GUI.skin.label) { alignment = TextAnchor.MiddleCenter, fontSize = 12 };
        var normalLabel = new GUIStyle(GUI.skin.label) { alignment = TextAnchor.MiddleCenter, fontSize = 11 };
        var foldOut = new GUIStyle(EditorStyles.foldout) { stretchWidth = true, fixedWidth = 1 };
        #endregion 



        GUILayout.Space(20);
        EditorGUILayout.LabelField("Control del Sistema de Eventos", Title);
        GUILayout.Space(20);
        EditorGUILayout.LabelField("", GUI.skin.horizontalSlider);
        GUILayout.Space(20);
        GUILayout.BeginHorizontal();
        EditorGUILayout.LabelField("Sistema de Eventos", Subheader);
        GUILayout.EndHorizontal();
        GUILayout.Space(10);
        GUILayout.BeginHorizontal();
        eventSystem = EditorGUILayout.ObjectField(eventSystem,typeof(EventSystem),true,GUILayout.ExpandWidth(true));
        GUILayout.EndHorizontal(); 

        base.OnInspectorGUI();
    }
}
