﻿using System.Collections; 
using UnityEngine;
using UnityEngine.PostProcessing;
using UnityEngine.UI;
using UnityEngine.Audio;
using UnityEditor;
using UnityEditor.SceneManagement;

[CustomEditor(typeof(SettingSystem))]
[System.Serializable]
public class SettingSystemEditor : Editor
{
    [SerializeField]
    SettingSystem scriptTarget;
    [SerializeField]
    Object camera, audioMixer, popUp, postProcessingLow, postProcessingMedium, postProcessingHigh, musicSlider, fxSlider, resolutionDropdown;
    [SerializeField]
    Object[] toggleGroup;
    [SerializeField]
    bool onMenu;
    [SerializeField]
    int graphicSettingIndex, currentResolutionIndex;
    [SerializeField]
    float musicVolume, fxVolume;
    bool postProcessing, resolution, audio, UI;


    private void OnEnable()
    {
        scriptTarget = (SettingSystem)target;
        camera = scriptTarget.mainCamera;
        audioMixer = scriptTarget.audioMixer;
        popUp = scriptTarget.panel;
        postProcessingLow = scriptTarget.postProcessingProfiles[0];
        postProcessingMedium = scriptTarget.postProcessingProfiles[1];
        postProcessingHigh = scriptTarget.postProcessingProfiles[2];
        musicSlider = scriptTarget.musicSlider;
        fxSlider = scriptTarget.fxSlider;
        resolutionDropdown = scriptTarget.resolutionDropdown;
        toggleGroup = scriptTarget.toggleGroup;
        onMenu = scriptTarget.onMenu; 
        graphicSettingIndex = scriptTarget.graphicSettings;
        musicVolume = scriptTarget.musicVolume;
        fxVolume = scriptTarget.fxVolume;
        currentResolutionIndex = scriptTarget.currentResolutionIndex;
        postProcessing = true;
        resolution = true;
        audio = true;
        UI = true;
    }
    public override void OnInspectorGUI()
    { 
        graphicSettingIndex = scriptTarget.graphicSettings;
        musicVolume = scriptTarget.musicVolume;
        fxVolume = scriptTarget.fxVolume;
        currentResolutionIndex = scriptTarget.currentResolutionIndex;
        #region Label Styles
        var Title = new GUIStyle(GUI.skin.label) { alignment = TextAnchor.MiddleCenter, fontSize = 14, fontStyle = FontStyle.Bold, wordWrap = false};
        var Subheader = new GUIStyle(GUI.skin.label) { alignment = TextAnchor.MiddleCenter, fontSize = 12 };
        var normalLabel = new GUIStyle(GUI.skin.label) { alignment = TextAnchor.MiddleCenter, fontSize = 11 };
        var foldOut = new GUIStyle(EditorStyles.foldout) { stretchWidth = true, fixedWidth = 1  };
        #endregion
        #region Title
        GUILayout.Space(20);
        EditorGUILayout.LabelField("Sistema de Configuraciones", Title);
        GUILayout.Space(20);
        EditorGUILayout.LabelField("", GUI.skin.horizontalSlider); 
        #endregion
        #region Graphics
        #region PostProcessing
        GUILayout.Space(20);
        GUILayout.BeginHorizontal(); 
        GUILayout.Label("Efectos de Postprocesado:", Subheader, GUILayout.ExpandWidth(true)); 
        GUILayout.EndHorizontal();
        GUILayout.BeginHorizontal();
        postProcessing = EditorGUILayout.Foldout(postProcessing, "", foldOut);
        GUILayout.EndHorizontal();
        if (postProcessing)
        {  
            GUILayout.BeginHorizontal();
            camera = EditorGUILayout.ObjectField(camera, typeof(GameObject), true, GUILayout.ExpandWidth(true));
            GUILayout.EndHorizontal();
            GUILayout.BeginHorizontal();
            GUILayout.Label("Index del Perfil:", normalLabel);
            GUILayout.EndHorizontal();
            GUILayout.BeginHorizontal();
            graphicSettingIndex = EditorGUILayout.IntField(graphicSettingIndex,GUILayout.ExpandWidth(true));
            GUILayout.EndHorizontal();
            GUILayout.Space(10);
            GUILayout.BeginHorizontal();
            GUILayout.BeginVertical(); 
            GUILayout.Label("0", normalLabel);
            postProcessingLow = EditorGUILayout.ObjectField(postProcessingLow, typeof(PostProcessingProfile), true, GUILayout.ExpandWidth(true));
            toggleGroup[0] = EditorGUILayout.ObjectField(toggleGroup[0], typeof(GameObject), true, GUILayout.ExpandWidth(true));
            GUILayout.EndVertical();
            GUILayout.BeginVertical();
            GUILayout.Label("1", normalLabel);
            postProcessingMedium = EditorGUILayout.ObjectField(postProcessingMedium, typeof(PostProcessingProfile), true, GUILayout.ExpandWidth(true));
            toggleGroup[1] = EditorGUILayout.ObjectField(toggleGroup[1], typeof(GameObject), true, GUILayout.ExpandWidth(true));
            GUILayout.EndVertical();
            GUILayout.BeginVertical();
            GUILayout.Label("2", normalLabel);
            postProcessingHigh = EditorGUILayout.ObjectField(postProcessingHigh, typeof(PostProcessingProfile), true, GUILayout.ExpandWidth(true));
            toggleGroup[2] = EditorGUILayout.ObjectField(toggleGroup[2], typeof(GameObject), true, GUILayout.ExpandWidth(true));
            GUILayout.EndVertical();
            
            GUILayout.EndHorizontal();
            GUILayout.Space(10);
        }
        EditorGUILayout.LabelField("", GUI.skin.horizontalSlider);
        #endregion
        #region Resolution
        GUILayout.Space(20);
        GUILayout.BeginHorizontal();
        GUILayout.Label("Resoluciones:", Subheader);
        GUILayout.EndHorizontal();
        GUILayout.BeginHorizontal();
        resolution = EditorGUILayout.Foldout(resolution, "", foldOut);
        GUILayout.EndHorizontal();
        if (resolution)
        {
            GUILayout.BeginHorizontal();
            resolutionDropdown = EditorGUILayout.ObjectField(resolutionDropdown, typeof(Dropdown), true, GUILayout.ExpandWidth(true));
            GUILayout.EndHorizontal();
            GUILayout.Space(10);
            GUILayout.Label("Index de Resolucion Actual:", Subheader);
            GUILayout.BeginHorizontal(); 
            currentResolutionIndex = EditorGUILayout.IntField(currentResolutionIndex);
            GUILayout.EndHorizontal();
            GUILayout.Space(10);
        }
        EditorGUILayout.LabelField("", GUI.skin.horizontalSlider);
        #endregion
        #region Audio
        GUILayout.Space(20);
        GUILayout.BeginHorizontal();
        GUILayout.Label("Audio:", Subheader);
        GUILayout.EndHorizontal();
        GUILayout.BeginHorizontal();
        audio = EditorGUILayout.Foldout(audio, "", foldOut);
        GUILayout.EndHorizontal();
        if (audio)
        {
            GUILayout.BeginHorizontal();
            audioMixer = EditorGUILayout.ObjectField(audioMixer, typeof(AudioMixer), true, GUILayout.ExpandWidth(true));
            GUILayout.EndHorizontal();
            GUILayout.BeginHorizontal();
            GUILayout.Label("Sliders:", normalLabel);
            GUILayout.EndHorizontal();
            GUILayout.BeginHorizontal();
            GUILayout.BeginVertical();
            GUILayout.Label("Music:", normalLabel);
            musicSlider = EditorGUILayout.ObjectField(musicSlider, typeof(Slider), true, GUILayout.ExpandWidth(true));
            musicVolume = EditorGUILayout.FloatField(musicVolume);
            GUILayout.EndVertical();
            GUILayout.Space(20);
            GUILayout.BeginVertical();
            GUILayout.Label("FX:", normalLabel);
            fxSlider = EditorGUILayout.ObjectField(fxSlider, typeof(Slider), true, GUILayout.ExpandWidth(true));
            fxVolume = EditorGUILayout.FloatField(fxVolume);
            GUILayout.EndVertical(); 
            GUILayout.EndHorizontal();
            GUILayout.Space(10);
        }
        EditorGUILayout.LabelField("", GUI.skin.horizontalSlider);
        #endregion
        #region UI
        GUILayout.Space(20);
        GUILayout.BeginHorizontal();
        GUILayout.Label("Interfaz de Usuario:", Subheader);
        GUILayout.EndHorizontal();
        GUILayout.BeginHorizontal();
        UI = EditorGUILayout.Foldout(UI, "", foldOut);
        GUILayout.EndHorizontal();
        if (UI)
        {
            GUILayout.BeginHorizontal();
            popUp = EditorGUILayout.ObjectField(popUp, typeof(GameObject), true, GUILayout.ExpandWidth(true));
            GUILayout.EndHorizontal();
            GUILayout.BeginHorizontal();
            GUILayout.Label("Menu?");
            onMenu = EditorGUILayout.Toggle(onMenu);
            GUILayout.EndHorizontal();
            GUILayout.Space(10);
        }
        EditorGUILayout.LabelField("", GUI.skin.horizontalSlider);
        #endregion
        #endregion

        if (GUI.changed)
        {
            EditorUtility.SetDirty(scriptTarget);
            EditorSceneManager.MarkSceneDirty(scriptTarget.gameObject.scene);
            ApplyChanges();
        } 
    }
    void ApplyChanges()
    {
        foreach (GameObject obj in Selection.gameObjects)
        {
            SettingSystem ss = obj.GetComponent<SettingSystem>();
            if (ss != null)
            { 
                ss.mainCamera = (GameObject)camera;
                ss.audioMixer = (AudioMixer)audioMixer;
                ss.panel = (GameObject)popUp;
                ss.postProcessingProfiles[0] = (PostProcessingProfile)postProcessingLow;
                ss.postProcessingProfiles[1] = (PostProcessingProfile)postProcessingMedium;
                ss.postProcessingProfiles[2] = (PostProcessingProfile)postProcessingHigh; 
                ss.musicSlider = (Slider)musicSlider;
                ss.fxSlider = (Slider)fxSlider;
                ss.resolutionDropdown = (Dropdown)resolutionDropdown;
                ss.toggleGroup[0] = (Toggle)toggleGroup[0];
                ss.toggleGroup[1] = (Toggle)toggleGroup[1];
                ss.toggleGroup[2] = (Toggle)toggleGroup[2];
                ss.onMenu = onMenu;
                ss.graphicSettings = graphicSettingIndex;
                ss.musicVolume = musicVolume;
                ss.fxVolume = fxVolume;
                ss.currentResolutionIndex = currentResolutionIndex;
            }
        }
    }
}
