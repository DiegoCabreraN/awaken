﻿using UnityEditor;
using UnityEngine;
using UnityEditor.SceneManagement;
using UnityEngine.EventSystems;

[CustomEditor(typeof(PlayerControllerSystem))]
[System.Serializable]

public class PlayerControllerSystemEditor : Editor
{
    [SerializeField]
    PlayerControllerSystem PCS;
    [SerializeField]
    Object player, camera, eventSystemManager, pauseMenu, instructionGUI, controllerSprite, keyboardSprite, masterSystem, gameOverScreen;
    [SerializeField]
    bool alive, canMove, a;

    public void OnEnable()
    {
        PCS = (PlayerControllerSystem)target;
        player = PCS.player;
        camera = PCS.camera;
        eventSystemManager = PCS.eventSystem;
        pauseMenu = PCS.pauseMenu;
        instructionGUI = PCS.instructions;
        controllerSprite = PCS.s[0];
        keyboardSprite = PCS.s[1];
        alive = PCS.alive;
        canMove = PCS.canmove;
        masterSystem = PCS.ms;
        gameOverScreen = PCS.gameOverScreen;
    }

    public override void OnInspectorGUI()
    {
         
        #region Label Styles
        var Title = new GUIStyle(GUI.skin.label) { alignment = TextAnchor.MiddleCenter, fontSize = 14, fontStyle = FontStyle.Bold, wordWrap = false };
        var Subheader = new GUIStyle(GUI.skin.label) { alignment = TextAnchor.MiddleCenter, fontSize = 12 };
        var normalLabel = new GUIStyle(GUI.skin.label) { alignment = TextAnchor.MiddleCenter, fontSize = 11 };
        var foldOut = new GUIStyle(EditorStyles.foldout) { stretchWidth = true, fixedWidth = 1 };
        #endregion
        var toogle = new GUIStyle(GUI.skin.toggle) { stretchHeight = true, stretchWidth = true, alignment = TextAnchor.MiddleCenter};



        GUILayout.Space(20);
        EditorGUILayout.LabelField("Sistema de Movimiento", Title);
        GUILayout.Space(20);
        EditorGUILayout.LabelField("", GUI.skin.horizontalSlider);


        EditorGUILayout.LabelField("Conexion al Sistema Maestro", Subheader);
        EditorGUILayout.BeginHorizontal();
        masterSystem = EditorGUILayout.ObjectField(masterSystem, typeof(MasterSystem), true, GUILayout.ExpandWidth(true)); 
        EditorGUILayout.EndHorizontal();
        GUILayout.Space(10);
        EditorGUILayout.LabelField("Jugador y Camara", Subheader);
        EditorGUILayout.BeginHorizontal(); 
        player = EditorGUILayout.ObjectField(player, typeof(GameObject), true, GUILayout.ExpandWidth(true)); 
        camera = EditorGUILayout.ObjectField(camera, typeof(GameObject), true, GUILayout.ExpandWidth(true));
        EditorGUILayout.EndHorizontal();
        GUILayout.Space(10);
        EditorGUILayout.LabelField("Sistemas de Eventos", Subheader);
        EditorGUILayout.BeginHorizontal();
        eventSystemManager = EditorGUILayout.ObjectField(eventSystemManager, typeof(EventSystemManager), true, GUILayout.ExpandWidth(true));
        EditorGUILayout.EndHorizontal();
        GUILayout.Space(10);
        EditorGUILayout.LabelField("GUI Conectada al Script", Subheader);
        EditorGUILayout.BeginHorizontal();
        pauseMenu = EditorGUILayout.ObjectField(pauseMenu, typeof(GameObject), true, GUILayout.ExpandWidth(true));
        instructionGUI = EditorGUILayout.ObjectField(instructionGUI, typeof(GameObject), true, GUILayout.ExpandWidth(true));
        EditorGUILayout.EndHorizontal();
        EditorGUILayout.BeginHorizontal();
        gameOverScreen = EditorGUILayout.ObjectField(gameOverScreen, typeof(GameObject), true, GUILayout.ExpandWidth(true));
        EditorGUILayout.EndHorizontal();
        GUILayout.Space(10);
        EditorGUILayout.LabelField("Iconos para botones", Subheader);
        EditorGUILayout.BeginHorizontal();
        controllerSprite = EditorGUILayout.ObjectField(controllerSprite, typeof(Sprite), true, GUILayout.ExpandWidth(true));
        keyboardSprite = EditorGUILayout.ObjectField(keyboardSprite, typeof(Sprite), true, GUILayout.ExpandWidth(true));
        EditorGUILayout.EndHorizontal();
        GUILayout.Space(10);
        EditorGUILayout.LabelField("Condicionales de Controlador", Subheader);
        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField("Jugable",GUILayout.Width(50));
        alive = EditorGUILayout.Toggle(true,toogle,GUILayout.Width(15));
        GUILayout.Space(40);
        EditorGUILayout.LabelField("Movimiento", GUILayout.Width(70));
        canMove = EditorGUILayout.Toggle(true, toogle, GUILayout.Width(15));
        EditorGUILayout.EndHorizontal();
        GUILayout.Space(10);

        if (GUI.changed)
        {
            EditorUtility.SetDirty(PCS);
            EditorSceneManager.MarkSceneDirty(PCS.gameObject.scene);
            ApplyChanges();
        }




        /*a = EditorGUILayout.Foldout(a, "Cambiar variables");
        if (a)
        {
            base.OnInspectorGUI();
        } */
    }
    void ApplyChanges()
    {
        foreach (GameObject obj in Selection.gameObjects)
        {
            PlayerControllerSystem ApplyPCS = obj.GetComponent<PlayerControllerSystem>();
            if (ApplyPCS != null)
            {
                ApplyPCS.alive = alive;
                ApplyPCS.canmove = canMove;
                ApplyPCS.player = (GameObject)player;
                ApplyPCS.camera = (GameObject)camera;
                ApplyPCS.eventSystem = (EventSystemManager)eventSystemManager;
                ApplyPCS.pauseMenu = (GameObject)pauseMenu;
                ApplyPCS.instructions = (GameObject)instructionGUI;
                ApplyPCS.s[0] = (Sprite)controllerSprite;
                ApplyPCS.s[1] = (Sprite)keyboardSprite;
                ApplyPCS.ms = (MasterSystem)masterSystem;
                ApplyPCS.gameOverScreen = (GameObject)gameOverScreen;
                Debug.Log("Changes"); 
            }
        }
    }
}
