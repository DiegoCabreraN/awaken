﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine; 

public class EventsTrigger : MonoBehaviour
{ 
    public EventTriggerObjective triggerObjective;
    public TriggerType triggerType;
    public GameObject[] objectToTransform;
    public GameObject buttonTop;
    public bool moving = false;
    public bool appear = false;
    public bool disappear = false;
    public bool destroyable = false;

    public enum EventTriggerObjective
    {
        OpenDoor,
        DisappearObject,
        AppearObject,
    };
    public enum TriggerType 
    {
        Lever,
        Button, 
    };
}
