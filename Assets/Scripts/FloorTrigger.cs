﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloorTrigger : MonoBehaviour
{
    public bool open;
    public bool block;
    public GameObject leftDoor, rightDoor;

    void Update()
    { 
        if (open && !block)
        { 
            leftDoor.transform.Rotate(new Vector3(0, 0, -90) * 2 * Time.deltaTime); 
            rightDoor.transform.Rotate(new Vector3(0, 0, -90) * 2 * Time.deltaTime);
            if (leftDoor.transform.rotation.eulerAngles.z <= 270 && leftDoor.transform.rotation.eulerAngles.z > 0)
            {
                block = true;
            } 
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        open = true;
    } 
}
