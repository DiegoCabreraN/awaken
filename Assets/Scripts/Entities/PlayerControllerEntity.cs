﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControllerEntity : MonoBehaviour
{
    public float movementSpeed;
    public float rotationalSpeed;
    public float runningSpeed;

    public float jumpForce;

    public MasterSystem masterSystem;
    protected float[] originalSpeeds;
    [HideInInspector]
    public bool jumpable;
    private float runMultiplier;

    private void Start()
    {
        originalSpeeds = new float[2];
        originalSpeeds[0] = movementSpeed;
        originalSpeeds[1] = rotationalSpeed; 
    }
    private void Update()
    {
        movementSpeed = masterSystem.pauseValue * runMultiplier * originalSpeeds[0];
        rotationalSpeed = masterSystem.pauseValue * runMultiplier * originalSpeeds[1];

        if (Input.GetButtonDown("Jump") && jumpable)
        {  
            this.gameObject.GetComponent<Rigidbody>().AddForce(Vector3.up * jumpForce,ForceMode.Impulse); 
        }
        if (Input.GetKey(KeyCode.LeftShift))
        {
            runMultiplier = runningSpeed;
        }
        else {
            runMultiplier = 1;
        }
    }


    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.CompareTag("DeadEnd"))
        {
            masterSystem.playerControl.Dead();
        }
        else if (collision.collider.CompareTag("Spear"))
        {
            if (this.transform.childCount > 0)
            {
                print(this.transform.GetChild(0).parent = null);
            }
            masterSystem.playerControl.Dead();
        }
    }
    private void OnCollisionStay(Collision collision)
    { 
        if (collision.collider.CompareTag("Floor"))
        {
            jumpable = true;
        } 
        else if (collision.collider.CompareTag("Platform")) {
            this.gameObject.transform.parent = collision.gameObject.transform;
            jumpable = true;
        }
    }
    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.CompareTag("Platform")) {
            this.gameObject.transform.parent = null;
        }
        jumpable = false;
    }
}
