﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorEvent : MonoBehaviour
{
    public Quaternion q;
    bool rot = false;
     
    private void Update()
    {
        if (rot && Mathf.Abs(this.gameObject.transform.rotation.z)>q.z) {
            Debug.Log("E");
            gameObject.transform.Rotate(new Vector3(90, 0, q.z * Time.deltaTime),Space.Self);
        }
    }


    public void OpenDoor(Vector3 v3) { 
        q.z = v3.z * q.z;
        rot = true;
    }
}
