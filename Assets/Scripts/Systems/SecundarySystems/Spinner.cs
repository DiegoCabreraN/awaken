﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spinner : MonoBehaviour
{
    public float speed;
    public Vector3 direction = new Vector3(0,0,1);
    void Update()
    {
        transform.Rotate(direction*speed*10*Time.deltaTime);
    }
}
