﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class MenuSelector : MonoBehaviour
{
    public GameObject continueButton, newGameButton;
    // Start is called before the first frame update
    void Start()
    {
        if (continueButton != null)
        {
            this.gameObject.GetComponent<EventSystem>().SetSelectedGameObject(continueButton);
        }
        else {
            this.gameObject.GetComponent<EventSystem>().SetSelectedGameObject(newGameButton);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
