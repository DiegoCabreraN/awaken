﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sinmove : MonoBehaviour
{ 
    private Vector3 originalPosition;
    public float paddingSize;
    public float paddingSpeed;

    void Start()
    {
        originalPosition = this.transform.position;    
    }

    void Update()
    {
        transform.position = originalPosition + new Vector3(0, Mathf.Sin(Time.time*paddingSpeed)*paddingSize, 0);
    }
}
