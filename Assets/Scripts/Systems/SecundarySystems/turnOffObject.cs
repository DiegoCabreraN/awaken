﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class turnOffObject : MonoBehaviour
{
    void TurnOff() { 
        this.gameObject.SetActive(false);
    }
}
