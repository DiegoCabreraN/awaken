﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems; 

public class FocusOnResume : MonoBehaviour
{ 
    public GameObject resume, continueButton;

    public void focusOnResume() {
        this.gameObject.GetComponent<EventSystem>().SetSelectedGameObject(resume); 
    }
    public void focusOnContinue() {
        this.gameObject.GetComponent<EventSystem>().SetSelectedGameObject(continueButton);
    }
}
