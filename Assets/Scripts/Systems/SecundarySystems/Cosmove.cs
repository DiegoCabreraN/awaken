﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cosmove : MonoBehaviour
{ 
    private Vector3 originalPosition; 
    public float paddingSize;
    public float paddingSpeed;
    public float rotationalMoment = 0; 
    public Vector3 direction;

    void Start()
    {
        originalPosition = this.transform.position;    
    }

    void Update()
    { 
        transform.position = originalPosition + new Vector3(direction.x* Mathf.Cos(Time.time*paddingSpeed)*paddingSize, direction.y * Mathf.Cos(Time.time*paddingSpeed) * paddingSize, direction.z * Mathf.Cos(Time.time*paddingSpeed) * paddingSize);
        transform.Rotate(Vector3.up*rotationalMoment);
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (!collision.collider.CompareTag("Player"))
        {
            this.GetComponent<Cosmove>().enabled = false;
        }
    }
    private void OnCollisionExit(Collision collision)
    {
        this.GetComponent<Cosmove>().enabled = true;
    }
}
