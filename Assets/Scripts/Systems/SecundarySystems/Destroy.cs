﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destroy : MonoBehaviour
{
    public int index;
    public void Disappear() {
        Destroy(this.gameObject);
    }
}
