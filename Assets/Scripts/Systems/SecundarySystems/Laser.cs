﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Laser : MonoBehaviour
{
    public MasterSystem ms;
    public GameObject canvas;
    LineRenderer line;

    void Start() {
        line = gameObject.GetComponent<LineRenderer>();
        line.enabled = true;
    }

    void Update()
    {
        
        Ray ray = new Ray(transform.position, transform.right);
        RaycastHit hit;
        line.SetPosition(0, ray.origin);
        if(Physics.Raycast(transform.position, transform.right, out hit))
        {
            if (hit.collider.CompareTag("Player")) {
                ms.GetComponent<PlayerControllerSystem>().alive = false;
                ms.GetComponent<PlayerControllerSystem>().player.GetComponent<Animator>().SetTrigger("dead");
                ms.eventSystem.es.GetComponent<FocusOnResume>().focusOnContinue();
                canvas.SetActive(true);
                canvas.GetComponent<Animator>().SetBool("fading",true); 

            }
            line.SetPosition(1, ray.GetPoint(hit.distance));
        }
        

    }
}
