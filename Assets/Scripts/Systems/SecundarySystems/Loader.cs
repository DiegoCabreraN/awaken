﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Loader : MonoBehaviour
{
    public GameObject Canvas;
    public MasterSystem ms;

    public void startAnim() {
        Canvas.SetActive(true);
        Cursor.visible=false;
        Time.timeScale = 1;
        this.gameObject.GetComponent<Animator>().SetBool("isPlaying", true);
    }
    public void turnOnControls() {
        ms.playerControl.canmove = true;
    }
    public void BackToMain() {
        LoadFadeLevel(2);
    }
    public void LoadFadeLevel()
    {
        SceneManager.LoadScene(1);
    }
    public void Reload() {
        PlayerPrefs.SetInt("load", 1);
        SceneManager.LoadScene(1);
    }
    public void LoadFadeLevel(int i) {
        if (i == 1)
        {
            PlayerPrefs.SetInt("load", 1);
            SceneManager.LoadScene(1);
        }
        else {
            SceneManager.LoadScene(0);
        }
    }
    public void TurnOffCanvas() { 
        Canvas.SetActive(false); 
    }
    public void FadeGameOver() {
        PlayerPrefs.SetInt("load", 1);
        SceneManager.LoadScene(1);
    }
    public void FadeGameOver(int i) { 
        this.gameObject.GetComponent<Animator>().SetBool("load", true);
    }
    public void reloadAnimation() {
        this.gameObject.GetComponent<Animator>().Play("LoadAnimation");
    }

    public void activeCursor() {
        Cursor.visible = true;
    } 
}
