﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

[System.Serializable]
public class PlayerControllerSystem : MonoBehaviour
{
    public GameObject player;
    public new GameObject camera;
    public EventSystemManager eventSystem;
    public MasterSystem ms;
    public EventSystem es;
    public GameObject pauseMenu;
    public GameObject instructions;

    public GameObject gameOverScreen;

    public Sprite[] s = new Sprite[2];
    public bool alive = true;
    public bool canmove = false;
    protected PlayerControllerEntity playerProperties;
    protected bool isPaused = false;
    protected bool controllerConnected;
    protected bool OpenDoor;
    protected bool ready = true; 

    void Start()
    {
        playerProperties = player.GetComponent<PlayerControllerEntity>();
        if (Input.GetJoystickNames().Length > 0) {
            if (Input.GetJoystickNames()[0].Length == 33 || Input.GetJoystickNames()[0].Length == 19) {
                controllerConnected = true;
            }
        }

    }

    void FixedUpdate()
    {

        #region PlayerMovement 

        if (canmove)
        {
            #region Controls 
            if (Input.GetAxis("Vertical") != 0 || Input.GetAxis("Horizontal") != 0)
            {
                player.transform.Translate(new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical")) * playerProperties.movementSpeed * Time.deltaTime, Space.Self);

            }
            if (Input.GetAxis("Mouse X") != 0 || Input.GetAxis("Mouse Y") != 0)
            {
                player.transform.Rotate(new Vector3(0, Input.GetAxis("Mouse X"), 0) * playerProperties.rotationalSpeed * Time.deltaTime, Space.Self);
                camera.transform.Rotate(new Vector3(-Input.GetAxis("Mouse Y"), 0, 0) * playerProperties.rotationalSpeed * Time.deltaTime, Space.Self);
                camera.transform.rotation = Quaternion.Euler(camera.transform.rotation.eulerAngles.x, camera.transform.rotation.eulerAngles.y, 0);
            }
            else if (Input.GetAxis("RotHorizontal") != 0 || Input.GetAxis("RotVertical") != 0)
            {
                player.transform.Rotate(new Vector3(0, Input.GetAxis("RotHorizontal"), 0) * playerProperties.rotationalSpeed * Time.deltaTime, Space.Self);
                camera.transform.Rotate(new Vector3(Input.GetAxis("RotVertical"), 0, 0) * playerProperties.rotationalSpeed * Time.deltaTime, Space.Self);
                camera.transform.rotation = Quaternion.Euler(camera.transform.rotation.eulerAngles.x, camera.transform.rotation.eulerAngles.y, 0);
            }
            else
            {
                player.transform.Rotate(new Vector3(0, 0, 0)); 
            } 

            #endregion
            #region AngleBlock
            if (camera.transform.rotation.eulerAngles.x < 290 && camera.transform.rotation.eulerAngles.x > 100)
            {
                camera.transform.rotation = Quaternion.Euler(290, camera.transform.rotation.eulerAngles.y, 0);
            }
            else if (camera.transform.rotation.eulerAngles.x > 75 && camera.transform.rotation.eulerAngles.x < 100)
            {
                camera.transform.rotation = Quaternion.Euler(75, camera.transform.rotation.eulerAngles.y, 0);
            }
            #endregion
        }
        #endregion

        if (!alive) {
            canmove = false;
        }
    }

    void Update()
    {
        if (alive)
        {
            #region Raycast
            RaycastHit hit;

            if (Physics.Raycast(camera.transform.position, camera.transform.forward, out hit))
            {
                if (hit.distance < 3 && !hit.collider.CompareTag("Untagged"))
                {
                    if (Input.GetJoystickNames().Length > 0)
                    {
                        instructions.GetComponentInChildren<Image>().sprite = s[0];
                    }
                    else
                    {
                        instructions.GetComponentInChildren<Image>().sprite = s[1];

                    }
                    if (hit.distance < 2 && hit.collider.CompareTag("collectable"))
                    {
                        instructions.SetActive(true);
                        if (Input.GetButtonDown("Grab"))
                        {
                            int eventIndex = hit.collider.GetComponent<Destroy>().index;
                            eventSystem.discoveredEvent[eventIndex] = true;
                            hit.collider.GetComponent<Destroy>().Disappear();
                            instructions.SetActive(false);
                        }
                    }
                    else if (hit.collider.CompareTag("Door")|| hit.collider.CompareTag("SecundaryDoor"))
                    { 
                        instructions.SetActive(true); 
                        if (Input.GetButtonDown("Grab"))
                        { 
                            if (hit.collider.CompareTag("SecundaryDoor"))
                            {
                                if (Mathf.Abs(hit.normal.x) == 1f)
                                {

                                    StartCoroutine(Door(-hit.normal.x, DoorUnitaryVector.i, hit.collider.gameObject));
                                }
                                else if (Mathf.Abs(hit.normal.z) == 1f)
                                {
                                    StartCoroutine(Door(-hit.normal.z, DoorUnitaryVector.k, hit.collider.gameObject));
                                }
                            }
                            else
                            {
                                if (Mathf.Abs(hit.normal.x) == 1f)
                                {

                                    StartCoroutine(Door(hit.normal.x, DoorUnitaryVector.i, hit.collider.gameObject));
                                }
                                else if (Mathf.Abs(hit.normal.z) == 1f)
                                {
                                    StartCoroutine(Door(hit.normal.z, DoorUnitaryVector.k, hit.collider.gameObject));
                                }
                            }
                            
                        }
                    }
                    else if (hit.collider.CompareTag("Lever"))
                    {
                        instructions.SetActive(true);
                        if (Input.GetButtonDown("Grab"))
                        {
                            if (hit.collider.gameObject.transform.rotation.x < -0.2) {
                                hit.collider.transform.Rotate(0, 0, 25);
                                hit.collider.gameObject.GetComponent<EventBehaviour>().StopMoving();
                            }
                            else
                            {
                                hit.collider.transform.Rotate(0, 0, -25);
                                hit.collider.gameObject.GetComponent<EventBehaviour>().StartMoving();
                            } 

                        }
                    }

                }
                else
                {
                    instructions.SetActive(false);

                }
            }

            #endregion

            #region PauseCall
            if (Input.GetButtonUp("Pause"))
            {
                CallPause(0);
            }
            else if (Input.GetButtonUp("Cancel"))
            {
                CallPause(1);
            }
            #endregion
        }
        if (Input.GetJoystickNames().Length > 0)
        {
            if (Input.GetJoystickNames()[0].Length == 33 || Input.GetJoystickNames()[0].Length == 19)
            {
                controllerConnected = true;
            }
        }

    }

    #region Pause
    public void CallPause(int i) {

        if (isPaused)
        {
            if (ms.SettingsMenu.activeSelf) {
                ms.settingSystem.GetComponent<SettingSystem>().LoadSettings();
                ms.SettingsMenu.SetActive(false);
            }
            ms.pauseValue = 1;
            Cursor.visible = false;
            pauseMenu.SetActive(false);
            isPaused = false;

        }
        else
        {
            if (i == 0)
            {
                Cursor.visible = true;
                ms.pauseValue = 0;
                pauseMenu.SetActive(true);
                isPaused = true;
            }
        }
    }
    #endregion

    #region Open Doors
    public IEnumerator Door(float val, DoorUnitaryVector a, GameObject door) {
        Animator animator = door.GetComponent<Animator>();
        if (animator.GetCurrentAnimatorStateInfo(0).IsName("EntryState"))
        {
            if (val > 0 && a == DoorUnitaryVector.i)
            {
                animator.SetFloat("Direction", 1f);
                animator.Play("Door i");
                yield return new WaitForSeconds(3);
                animator.SetFloat("Direction", -2f);
                animator.Play("Door i", -1, float.NegativeInfinity);
                yield return new WaitForSeconds(2);
                animator.SetFloat("Direction", 1f);
                animator.Play("EntryState");
            }
            else if (val < 0 && a == DoorUnitaryVector.i)
            {
                animator.SetFloat("Direction", 1f);
                animator.Play("Door noti");
                yield return new WaitForSeconds(3);
                animator.SetFloat("Direction", -2f);
                animator.Play("Door noti", -1, float.NegativeInfinity);
                yield return new WaitForSeconds(2);
                animator.SetFloat("Direction", 1f);
                animator.Play("EntryState");
            }
            else if (val > 0 && a == DoorUnitaryVector.k)
            {
                animator.SetFloat("Direction", 1f);
                animator.Play("Door k");
                yield return new WaitForSeconds(3);
                animator.SetFloat("Direction", -2f);
                animator.Play("Door k", -1, float.NegativeInfinity);
                yield return new WaitForSeconds(2);
                animator.SetFloat("Direction", 1f);
                animator.Play("EntryState");
            }
            else if (val < 0 && a == DoorUnitaryVector.k)
            {
                animator.SetFloat("Direction", 1f);
                animator.Play("Door notk");
                yield return new WaitForSeconds(3);
                animator.SetFloat("Direction", -2f);
                animator.Play("Door notk", -1, float.NegativeInfinity);
                yield return new WaitForSeconds(2);
                animator.SetFloat("Direction", 1f);
                animator.Play("EntryState");

            }
        }
        

    }
    public enum DoorUnitaryVector{
        k,
        i
    }
    #endregion

    #region Dead
    public void Dead()
    {
        alive = false;
        player.GetComponent<push>().enabled = true;
        player.GetComponent<Animator>().SetTrigger("dead");
        ms.eventSystem.es.GetComponent<FocusOnResume>().focusOnContinue();
        
        gameOverScreen.SetActive(true);
        gameOverScreen.GetComponent<Animator>().SetBool("fading", true);
    }
    #endregion

     
}
