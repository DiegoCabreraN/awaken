﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
 
public class EventSystemManager : MonoBehaviour
{
    [Header("Sistemas Escenciales ")]


    [Header("Control para sistemas de Eventos")]
    [HideInInspector]
    public EventSystem es;  
    [Header("Eventos Descubiertos")]
    [Tooltip("Aqui van los eventos que han sido triggereados")] 
    public bool[] discoveredEvent;
    [Header("Objetos Descubiertos")]
    [Tooltip("Aqui van los coleccionables que han sido encontrados")] 
    public GameObject[] collectionObjects;
    public GameObject platforms, button;


    void Update()
    {
        if(discoveredEvent[5] && !platforms.activeInHierarchy) {
            button.SetActive(false);
            platforms.SetActive(true);
        }
    }
    

    public void run()
    {
        for (int i = 0; i < discoveredEvent.Length - 1; i++)
        {
            if (discoveredEvent[i])
            {
                Destroy(collectionObjects[i]);
            }
        }
    }
    public void discoverEvent(int i)
    {
        discoveredEvent[i] = true;

    }
      
    
}
