﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainManager : MonoBehaviour
{
    public GameObject SettingsMenu;
    public GameObject canvas;
    public GameObject continueButton; 
    public GameObject fadeInObject;
    public Animator animator;
    private GameObject activedSettingsMenu;


    void Awake()
    { 
        if (!SaveSystem.FilePathExistence())
        { 
            Destroy(continueButton);  
        }
        
    }
    void Start() {
        Cursor.visible = true;
    }
     
    public void openSettings() {
        if (SettingsMenu.activeSelf)
        {
            SettingsMenu.transform.parent.GetComponentInChildren<SettingSystem>().LoadSettings();
            SettingsMenu.transform.parent.GetComponentInChildren<SettingSystem>().Configure();
            SettingsMenu.SetActive(false);

        }
        else
        {
            SettingsMenu.SetActive(true);
            SettingsMenu.transform.parent.GetComponentInChildren<SettingSystem>().StartLoad();
        }

    } 
    public void newGame()
    {
        // Cinematica de Entrada
        fadeInObject.SetActive(true);
        animator.SetBool("isPlaying", true); 
    }
    public void continueGame() {
        fadeInObject.SetActive(true);
        animator.SetBool("isPlaying",true);
        PlayerPrefs.SetInt("load", 1);    
    } 
    public void closeGame() {
        Application.Quit();
    }
}
