﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

[System.Serializable]
public class MasterSystem : MonoBehaviour
{
    [Header("Control de Sistemas")]

    [Tooltip("Sistema de Movimiento para Jugador")] 

    public PlayerControllerSystem playerControl;

    [Tooltip("Sistema de Configuraciones")] 
    public GameObject SettingsMenu;
    public SettingSystem settingSystem;

    [Tooltip("Sistema de Eventos")] 
    public EventSystemManager eventSystem; 
     
    [HideInInspector] 
    public float pauseValue;

    void Awake()
    {
        #region LoadGame
        Cursor.visible = false;
        if (PlayerPrefs.GetInt("load") == 1) {
            Debug.Log(PlayerPrefs.GetInt("load"));
            LoadData();
            PlayerPrefs.SetInt("Load", 0);
            PlayerPrefs.DeleteKey("load");
        }
        #endregion
    }


    void Update()
    { 
        eventSystem.run(); 
    }

    public void SaveGame()
    {
        SaveSystem.SavePlayer(this);
        settingSystem.panel.GetComponentInChildren<Text>().text = "game saved";
        settingSystem.panel.SetActive(true);
        Debug.Log("Saved");
    }
    public void openSettings()
    {
        if (SettingsMenu.activeSelf)
        {
            settingSystem.LoadSettings(); 
            SettingsMenu.SetActive(false);

        }
        else {
            SettingsMenu.SetActive(true);
            settingSystem.StartLoad();
        }
    }
    public void LoadData() {
        PlayerData data = SaveSystem.LoadPlayer();
        Vector3 position;
        position.x = data.position[0];
        position.y = data.position[1];
        position.z = data.position[2];
        playerControl.player.transform.position = position;
        Quaternion rotation;
        rotation.x = data.rotation[0];
        rotation.y = data.rotation[1];
        rotation.z = data.rotation[2];
        rotation.w = data.rotation[3];
        playerControl.player.transform.rotation = rotation;
        for (int i = 0; i < data.size; i++)
        {
            eventSystem.discoveredEvent[i] = data.discoveredEvents[i];
        }
        Debug.Log("Loaded");
    }

    public void QuitGame() {
        Application.Quit();
        Debug.Log("Exit");
    }

    
}
