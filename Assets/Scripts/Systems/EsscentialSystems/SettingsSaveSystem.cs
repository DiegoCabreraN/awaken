﻿using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public static class SettingsSaveSystem{
    private static string settingsPath = Application.persistentDataPath + "/settings.data";

    public static bool FileSettingsExistence()
    {
        if (File.Exists(settingsPath))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public static void SaveSettings(SettingSystem settingSystem)
    {
        Debug.Log(settingSystem.graphicSettings);
        BinaryFormatter formatter = new BinaryFormatter();
        FileStream stream = new FileStream(settingsPath, FileMode.Create);

        SettingsData data = new SettingsData(settingSystem);
        formatter.Serialize(stream, data);
        stream.Close();
    }
    public static SettingsData LoadSettings()
    {
        if (File.Exists(settingsPath))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(settingsPath, FileMode.Open);
            SettingsData data = formatter.Deserialize(stream) as SettingsData;
            stream.Close();
            return data;
        }
        else
        {
            Debug.LogError("PathNotFound");
            return null;
        }

    }
}
