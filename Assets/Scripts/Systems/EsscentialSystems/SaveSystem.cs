﻿using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public static class SaveSystem
{
    private static string path = Application.persistentDataPath + "/player.data";
    private static string settingsPath = Application.persistentDataPath + "/settings.data";

    public static void SavePlayer(MasterSystem masterSystem) {
        BinaryFormatter formatter = new BinaryFormatter();
        FileStream stream = new FileStream(path, FileMode.Create);

        PlayerData data = new PlayerData(masterSystem);
        formatter.Serialize(stream, data);
        stream.Close();
        Debug.Log("Game Saved");
    }

    public static PlayerData LoadPlayer() { 
        if (File.Exists(path))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(path, FileMode.Open);
            PlayerData data = formatter.Deserialize(stream) as PlayerData;
            stream.Close();
            return data;
        }
        else {
            Debug.LogError("File not found in " + path);
            return null;
        }

    }

    public static bool FilePathExistence()
    {
        if (File.Exists(path))
        {
            return true;
        }
        else
        {
            return false; 
        }
    } 
}
