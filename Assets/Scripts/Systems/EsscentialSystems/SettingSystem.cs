﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.PostProcessing;
using UnityEngine.UI;
using UnityEngine.Audio;

public class SettingSystem : MonoBehaviour
{
    public GameObject mainCamera;
    public AudioMixer audioMixer;
    public GameObject panel;
    public PostProcessingProfile[] postProcessingProfiles = new PostProcessingProfile[3]; 
    public Slider musicSlider; 
    public Slider fxSlider;
    public bool onMenu;
    public Toggle[] toggleGroup; 
    public int graphicSettings = 0;
    public float musicVolume = 50; 
    public float fxVolume = 80;
    Resolution[] resolutions;
    public Dropdown resolutionDropdown;
    public int currentResolutionIndex = 0;
    public bool fullscreen = true;

    public void StartLoad() 
    {
        if (SettingsSaveSystem.FileSettingsExistence())
        {
            LoadSettings(); 
        }
        else {
            SaveSettings(); 
        } 
    }
    public void Start()
    {
        StartLoad();
        resolutions = Screen.resolutions;
        resolutionDropdown.ClearOptions();
        List<string> res = new List<string>(); 

        for (int i = 0; i < resolutions.Length; i++) {
            string option = resolutions[i].width + " x " + resolutions[i].height;
            res.Add(option);
            if (resolutions[i].height == Screen.currentResolution.height && resolutions[i].width == Screen.currentResolution.width) {
                currentResolutionIndex = i;
            }
        }
        resolutionDropdown.AddOptions(res);
        resolutionDropdown.value = currentResolutionIndex;
        resolutionDropdown.RefreshShownValue();

        
    }

    public void run()
    {
        LoadSettings();
        Configure(); 
    }

    void Update() { 
        musicSlider.onValueChanged.AddListener(delegate{ changeMusic(musicSlider.value); });
        fxSlider.onValueChanged.AddListener(delegate { changeFx(fxSlider.value); });
        
        Configure();
    }
     
    public void SaveSettings()
    {
        if (panel.activeSelf)
        {
            panel.GetComponent<Animator>().Play("display", -1, 0f);
        }
        else
        {
            panel.GetComponentInChildren<Text>().text = "graphics saved";
            panel.SetActive(true);
        } 
        SettingsSaveSystem.SaveSettings(this.gameObject.GetComponent<SettingSystem>());
        Configure();
    }
    public void LoadSettings() {
        SettingsData set = SettingsSaveSystem.LoadSettings();
        graphicSettings = set.graphicConfiguration;
        musicVolume = set.musicVolume;
        fxVolume = set.fxVolume;
        currentResolutionIndex = set.resolutionIndex;
        fullscreen = set.fullscreen;
    }
    protected void changeMusic(float a)
    {
        musicVolume = a;
        audioMixer.SetFloat("MusicVolume",a);
    }
    protected void changeFx(float a)
    {
        fxVolume = a;
    }
    public void SetResolution(int i) {
        Screen.SetResolution(resolutions[i].width, resolutions[i].height, true);
        currentResolutionIndex = i;
        Debug.Log(Screen.currentResolution);
    }
    public void Configure() {
        for (int i = 0; i < toggleGroup.Length; i++)
        {
            if (toggleGroup[i].isOn)
            {
                if (i != graphicSettings)
                {
                    graphicSettings = i;
                }
            }
        }
        if (!onMenu)
        {
            fxSlider.value = fxVolume;
            musicSlider.value = musicVolume;  
            toggleGroup[graphicSettings].isOn = true;
            QualitySettings.SetQualityLevel(graphicSettings); 
        }
        else {
            fxSlider.value = fxVolume;
            musicSlider.value = musicVolume;
            toggleGroup[graphicSettings].isOn = true;
            QualitySettings.SetQualityLevel(graphicSettings); 
        }
    } 
}
