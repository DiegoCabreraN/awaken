﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SettingsData
{
    public int graphicConfiguration;
    public float musicVolume;
    public float fxVolume;
    public int resolutionIndex;
    public bool fullscreen;

    public SettingsData(SettingSystem settingSystem)
    {
        graphicConfiguration = settingSystem.graphicSettings;
        musicVolume = settingSystem.musicVolume;
        fxVolume = settingSystem.fxVolume;
        resolutionIndex = settingSystem.currentResolutionIndex;
        fullscreen = settingSystem.fullscreen;
    } 
}

