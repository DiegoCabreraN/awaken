﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PlayerData
{
    public int size;
    public bool[] discoveredEvents;
    public float[] position;
    public float[] rotation;

    public PlayerData(MasterSystem masterSystem) {
        size = masterSystem.eventSystem.discoveredEvent.Length; 

        position = new float[3];
        position[0] = masterSystem.playerControl.player.transform.position.x;
        position[1] = masterSystem.playerControl.player.transform.position.y;
        position[2] = masterSystem.playerControl.player.transform.position.z;

        rotation = new float[4];
        rotation[0] = masterSystem.playerControl.player.transform.rotation.x;
        rotation[1] = masterSystem.playerControl.player.transform.rotation.y;
        rotation[2] = masterSystem.playerControl.player.transform.rotation.z;
        rotation[3] = masterSystem.playerControl.player.transform.rotation.w;

        discoveredEvents = new bool[size];

        for (int i = 0; i < size; i++) { 
            discoveredEvents[i] = masterSystem.eventSystem.discoveredEvent[i]; 
        }
    }
}
